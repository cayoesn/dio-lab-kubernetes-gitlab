!["dio_logo"](https://digitalinnovationone.github.io/roadmaps/assets/logo-dio.svg)

# dio-lab-kubernetes-gitlab
Repositório do Desafio DIO - Criando um Pipeline de Deploy com GitLab e Kubernetes

Neste desafio nós construimos um deploy de uma aplicação utilizando o Kubernetes e fizemos a pipeline de integração contínua com o GitLab.